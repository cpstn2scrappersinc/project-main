﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHandScript : MonoBehaviour {

    public Player player;

    private void Start()
    {
        player = GameObject.FindWithTag("Player").GetComponent<Player>();
    }

    // Collider trigger functions
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Interactable>() && !player.isHandsFull)
            player.currentObject = other.gameObject;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<Interactable>())
            player.currentObject = null;
    }
}
