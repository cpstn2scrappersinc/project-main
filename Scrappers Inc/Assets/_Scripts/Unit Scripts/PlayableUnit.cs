﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayableUnit : MonoBehaviour {

    // The unit's rigidbody
    public Rigidbody rb;

    // Unit speed variables
    public float runSpeed;
    public float walkSpeed;

    // Unit's current speed
    float curSpeed;

    // Transform where the unit places the object it's picked up (box, mounted player...)
    public GameObject playerPickUpSpace;
    public bool isHandsFull;

    // Boolean value for mounting
    public bool isMounted = false;

    // Current interactable object
    public GameObject currentObject;

    // Boolean value. Shows true when the unit reaches the end goal
    public bool isAtEnd;

    // The Unit's type
    public UnitType unitType;
    public enum UnitType
    {
        Player,
        Spider,
    }

    // Animator controller variables
    public Animator unitAnim;

    // Command for E key
    public virtual void ECommand()
    {
    }

    // Command for spacebar
    public virtual void SpacebarCommand()
    {

    }

    public virtual void UpdateMoveAnim(Vector3 dir)
    {
        if (dir.x == 0 && dir.y == 0)
            unitAnim.SetBool("isMoving", false);
        else
            unitAnim.SetBool("isMoving", true);
        
        unitAnim.SetFloat("velocityX", dir.x);
        unitAnim.SetFloat("velocityZ", dir.z);
    }

    // Movement script
    public virtual void Move()
    {
        bool running = Input.GetKey(KeyCode.LeftShift);

        float hAxis = Input.GetAxis("Horizontal");
        float vAxis = Input.GetAxis("Vertical");
        curSpeed = (running) ? runSpeed : walkSpeed;

        Vector3 movement = (transform.forward * vAxis) + (transform.right * hAxis);
        rb.transform.position += movement * curSpeed * Time.deltaTime;

        Vector3 heading = Vector3.Normalize(movement);

        UpdateMoveAnim(heading);

        /*
        Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        Vector2 inputDir = input.normalized;

        if (inputDir != Vector2.zero)
        {
            float targetRotation = Mathf.Atan2(inputDir.x, inputDir.y) * Mathf.Rad2Deg;
            transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRotation, ref turnSmoothVelocity, turnSmoothTime);
        }

        bool running = Input.GetKey(KeyCode.LeftShift);
        float targetSpeed = ((running) ? runSpeed : walkSpeed) * inputDir.magnitude;
        curSpeed = Mathf.SmoothDamp(curSpeed, targetSpeed, ref speedSmoothVelocity, speedSmoothTime);
        transform.Translate(transform.forward * curSpeed * Time.deltaTime, Space.World);
        */
    }
}
