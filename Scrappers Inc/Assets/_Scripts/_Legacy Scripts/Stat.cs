﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using UnityEngine;

[Serializable]
public class Stat 
{
	// Base value of the stat
	public float baseValue;

	// Boolean to determine if a modifier is added/removed. Keeps script from checking for stat changes all the time
	protected bool isDirty = true;

	// Container for final val
	protected float _value;

	// List of modifiers affecting the stat
	protected readonly List<StatModifier> statMods;

	public readonly ReadOnlyCollection<StatModifier> statModifiers;

	// Stat constructors
	public Stat()
	{
		statMods = new List<StatModifier>();
		statModifiers = statMods.AsReadOnly(); // Add this line to the constructor
	}
	public Stat(float baseVal) : this()
	{
		baseValue = baseVal;
	}

	//Saves the last base value just in case
	protected float lastBaseValue = float.MinValue;

	// Gets the final value of the stat (base + all modifiers)
	public virtual float finalValue  { get {
			if(isDirty || lastBaseValue != baseValue) 
			{
				lastBaseValue = baseValue;
				_value = CalculateFinalVal();
				isDirty = false;
			}
			return _value;
		}
	}

	// Sorts the modifiers based on type (flat mods, percentages, multipliers...)
	protected virtual int CompareModifierOrder(StatModifier a, StatModifier b)
	{
		if (a.statOrder < b.statOrder)
			return -1;
		else if (a.statOrder > b.statOrder)
			return 1;
		return 0; // if (a.Order == b.Order)
	}

	// Adds a modifier
	public virtual void AddStatMod(StatModifier mod)
	{
		isDirty = true;
		statMods.Add (mod);
		statMods.Sort(CompareModifierOrder);
	}

	// Removes a modifier
	public virtual bool RemoveStatMod(StatModifier mod)
	{
		if (statMods.Remove(mod))
		{
			isDirty = true;
			return true;
		}
		return false;
	}

	// Removes all modifiers that came from a source, all at once
	public virtual bool RemoveAllModifiersFromSource(object source)
	{
		bool didRemove = false;

		for (int i = statMods.Count - 1; i >= 0; i--)
		{
			if (statMods[i].statSource == source)
			{
				isDirty = true;
				didRemove = true;
				statMods.RemoveAt(i);
			}
		}
		return didRemove;
	}

	// Calculates the final value of the stat
	protected virtual float CalculateFinalVal ()
	{
		// Initial float value
		float finalVal = baseValue;

		// For every modifier in the list (AKA for every modifier affecting this stat
		for (int i = 0; i < statMods.Count; i++)
		{
			StatModifier mod = statMods[i];
			float sumPercentAdd = 0; // Holds the sum of our "PercentAdd" modifiers

			if (mod.statType == StatModType.Flat)  // If the modifier is a flat number, add it
			{
				finalVal += mod.statVal;
			}
			else if (mod.statType == StatModType.PercentAdd) // When we encounter a "PercentAdd" modifier
			{
				sumPercentAdd += mod.statVal; // Start adding together all modifiers of this type

				// If we're at the end of the list OR the next modifer isn't of this type
				if (i + 1 >= statMods.Count || statMods[i + 1].statType != StatModType.PercentAdd)
				{
					finalVal *= 1 + sumPercentAdd; // Multiply the sum with the "finalValue", like we do for "PercentMult" modifiers
					sumPercentAdd = 0; // Reset the sum back to 0
				}
			}
			else if (mod.statType == StatModType.PercentMult)  // Else if the modifier is a multiplier, multiply it
			{
				finalVal *= 1 + mod.statVal;
			}
		}

		// Return the final value, rounded for accuracy
		return (float)Mathf.Round (finalVal);
	}
}