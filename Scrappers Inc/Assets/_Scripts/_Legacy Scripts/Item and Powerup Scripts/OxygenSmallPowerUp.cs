﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OxygenSmallPowerUp : PowerUp {

	public override void PickupEffect(Collider other)
	{
		other.gameObject.GetComponent<PlayerScript> ().curOxygen += pickupValue;
		base.PickupEffect (other);
	}
}
