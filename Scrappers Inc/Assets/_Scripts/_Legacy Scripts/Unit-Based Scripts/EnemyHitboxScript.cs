﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHitboxScript : MonoBehaviour {

	// Damage multiplier, when enemy is struck in sides or back. Set in inspector.
	public float multiplier;

	void OnTriggerEnter(Collider other)
	{
		// Damages the enemy and destroys the projectile. TEMP: Base damage is set to 10 until weapon stats come out
		if (other.tag == "Projectile") 
		{
			this.GetComponentInParent<Unit> ().DamageHP (10 * (1.0f + multiplier));
			Destroy (other.gameObject);
		}
	}
}
