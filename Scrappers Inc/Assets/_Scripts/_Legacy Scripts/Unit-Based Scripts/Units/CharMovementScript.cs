﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharMovementScript : MonoBehaviour {
	
	// Physics variables
	Vector3 movement;
	Rigidbody playerRigidbody;

	public GameObject currentActiveChar;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update()
	{
		Move ();
	}

	// Moves the player via direction keys
	void Move ()
	{
        currentActiveChar.transform.Translate(8 * Input.GetAxis("Horizontal") * Time.deltaTime, 0f, 8 * Input.GetAxis("Vertical") * Time.deltaTime);
	}
}
