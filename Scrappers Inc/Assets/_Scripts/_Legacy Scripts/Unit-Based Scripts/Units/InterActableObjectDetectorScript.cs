﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterActableObjectDetectorScript : MonoBehaviour {

	public GameObject interactableSwitch;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		Interact ();
	}

	void Interact ()
	{
		if (Input.GetKeyDown (KeyCode.E) && interactableSwitch != null)
		{
			interactableSwitch.GetComponent<DoorWallSwitchScript>().activateSwitch();
		}
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.GetComponent<DoorWallSwitchScript>()) 
		{
			interactableSwitch = other.gameObject;
		}
	}

	void OnTriggerExit (Collider other)
	{
		if (other.gameObject.GetComponent<DoorWallSwitchScript>()) 
		{
			interactableSwitch = null;
		}
	}
}
