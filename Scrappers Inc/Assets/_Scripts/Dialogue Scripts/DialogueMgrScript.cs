﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueMgrScript : MonoBehaviour {

	public List<string> puzzleDialogues;
	public List<int> dialogueDurations;
	public Text subtitles;

	public IEnumerator displayDialogue (int dialogueIndex)
	{
		subtitles.text = "";
		for (int i = 0; i < puzzleDialogues [dialogueIndex].Length; i++) 
		{
			subtitles.text += puzzleDialogues [dialogueIndex][i];
			yield return new WaitForSeconds(0.005f);
		}
		yield return new WaitForSeconds(dialogueDurations[dialogueIndex]);
		subtitles.text = "";
	}
}
