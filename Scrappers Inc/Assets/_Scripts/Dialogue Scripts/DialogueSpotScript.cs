﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueSpotScript : MonoBehaviour 
{
	public GameObject dialogueMgr;
	public int dialogueIndex;
	bool dialogueDisplayed;

	public void DisplayDialogue()
	{
		StartCoroutine(dialogueMgr.GetComponent<DialogueMgrScript>().displayDialogue(dialogueIndex));
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.GetComponent<PlayableUnit> () && dialogueDisplayed == false) 
		{
			StopCoroutine(dialogueMgr.GetComponent<DialogueMgrScript>().displayDialogue(dialogueIndex));
			DisplayDialogue();
			dialogueDisplayed = true;
		}
	}
}
