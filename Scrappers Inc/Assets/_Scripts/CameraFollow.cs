﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject target;
    public float rotateSpeed = 5;

    Vector3 offset;

    void Start()
    {
        target = GameObject.FindWithTag("Player");
        offset = target.transform.position - transform.position;
    }

    // Moves the camera based on mouse position and rotates the player as well
    void LateUpdate()
    {
        float horizontal = Input.GetAxisRaw("Mouse X") * rotateSpeed;
        float vertical = Input.GetAxisRaw("Mouse Y") * rotateSpeed;
        target.transform.Rotate(0, horizontal, 0);

        float desiredAngle = target.transform.eulerAngles.y;
        Quaternion rotation = Quaternion.Euler(0, desiredAngle, 0);

        transform.position = target.transform.position - (rotation * offset);

        transform.LookAt(target.transform);
    }

    // Sets the new target of the camera, to be used by CharMgrScript
    public void SetTarget(GameObject newTarget)
    {
        target = newTarget;
    }
}


// ===OLD CODE===
/*
*/

//public Transform target;            // The position that that camera will be following.
//public float smoothing = 5f;        // The speed with which the camera will be following.

//Vector3 offset;                     // The initial offset from the target.

//void Start ()
//{
//	// Calculate the initial offset.
//	offset = transform.position - target.position;
//}

//void FixedUpdate ()
//{
//	// Create a postion the camera is aiming for based on the offset from the target.
//	Vector3 targetCamPos = target.position + offset;

//	// Smoothly interpolate between the camera's current position and it's target position.
//	transform.position = Vector3.Lerp (transform.position, targetCamPos, smoothing * Time.deltaTime);
//}