﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneMgrScript : MonoBehaviour {

	public string[] scenes;

	public void LoadScene(int sceneNo)
	{
		SceneManager.LoadScene(scenes[sceneNo]);
	}
}
