﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerraformerScript : MonoBehaviour
{
    public List<PressurePlate> floorSwitches = new List<PressurePlate>();
    public Camera oldCam;
    public Camera newCam;
    public GameObject sphere;

    CharMgrScript charMgr;
    GameController gamCon;

    public bool isStarted = false;

    IEnumerator terraformCoroutine;

    // Use this for initialization
    void Start ()
    {
        charMgr = GameObject.Find("Character Manager").GetComponent<CharMgrScript>();
        gamCon = GameObject.Find("GameController").GetComponent<GameController>();
        terraformCoroutine = MoveCam(10);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (StartTerraformer() && !isStarted)
        {
            Debug.Log("STARTED");
            Terraform();
        }
	}

    // If the plates are being pressed on, start terraforming sequence
    bool StartTerraformer()
    {
        foreach (PressurePlate plate in floorSwitches)
            if (plate.isPressed == false)
                return false;

        return true;
    }


    // Moves the new camera backwards
    IEnumerator MoveCam(float time)
    {
        float i = 0;
        float rate = 1 / time;

        while (i < 3)
        {
            i += Time.deltaTime;
            newCam.transform.position = new Vector3(newCam.transform.position.x, newCam.transform.position.y + rate, newCam.transform.position.z + (rate *-1));

            if (i > 1)
                sphere.transform.localScale += Vector3.one * rate * 10;

            yield return 0;
        }

        yield return new WaitForSeconds(3);
        gamCon.ChangeScene();
        StopCoroutine(terraformCoroutine);
    }

    // The terraformation script, which starts the cutscene
    void Terraform()
    {
        isStarted = true;
        SetCameras();
        charMgr.StopAllCharAnims();
        StartCoroutine(terraformCoroutine);
    }

    // Activates the new camera and turns off the old
    void SetCameras()
    {
        oldCam.gameObject.SetActive(false);
        newCam.gameObject.SetActive(true);
    }
}
