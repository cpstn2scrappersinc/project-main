﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanTeleporterScript : TeleporterBaseScript {

	public GameObject spiderTeleporter;
	public GameObject gameController;

	public void CheckTeleporters ()
	{
		if (activated == true && spiderTeleporter.GetComponent<SpiderTeleporterScript> ().activated == true) 
		{
			gameController.GetComponent<GameController>().ChangeScene();
			Debug.Log("checkingteleporters");
		}
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.GetComponent<Player> ()) 
		{
			SwitchTeleporter(true);
			SwitchLights(true);
			CheckTeleporters();
		}
	}
	void OnTriggerExit (Collider other)
	{
		if (other.GetComponent<Player> ()) 
		{
			SwitchTeleporter(false);
			SwitchLights(false);
			//CheckTeleporters();
		}
	}
}
