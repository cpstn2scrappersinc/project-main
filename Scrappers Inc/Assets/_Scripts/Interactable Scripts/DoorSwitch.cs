﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorSwitch : Interactable {

    public bool isTimedSwitch;
    public bool isOn;

	// Use this for initialization
	void Start ()
    {
        canInteract = true;
        helpSigns = GetComponentInChildren<Canvas>();
    }
	
	// Update is called once per frame
	void Update ()
    {
	}

    public override void Interact()
    {
        base.Interact();

        if (isTimedSwitch)
            isOn = true;
        else
        {
            //Toggles door to open close via Retractable component(temp)
            linkedObject.GetComponent<Retractable>().ToggleRetraction();
        }
    }
}
