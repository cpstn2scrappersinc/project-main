﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpiderMounting : Interactable {

    Spider spider;
    Player player;
    bool showHelpSign = true;

	// Use this for initialization
	void Start ()
    {
        canInteract = true;
        player = GameObject.FindWithTag("Player").GetComponent<Player> ();
        spider = GameObject.FindWithTag("Spider").GetComponent<Spider>();
        helpSigns = GetComponentInChildren<Canvas>();
    }
	
	// Update is called once per frame
	void Update ()
    {
    }

    // Mounting mechanic
    public override void Interact()
    {
        base.Interact();

        player.gameObject.transform.parent = spider.playerPickUpSpace.transform;
        player.gameObject.transform.localPosition = new Vector3(0, 0, 0);
        player.gameObject.transform.localRotation = Quaternion.identity;
        player.gameObject.GetComponent<Rigidbody>().isKinematic = true;
        player.gameObject.GetComponent<Rigidbody>().useGravity = false;
        player.isMounted = true;
    }

    public override void ShowHelpSigns()
    {
        if (player.isMounted || GameObject.Find("GameController").GetComponentInChildren<CharMgrScript>().activeCharacter == this.gameObject)
            showHelpSign = false;
        else showHelpSign = true;
        
        base.ShowHelpSigns();
    }
}
