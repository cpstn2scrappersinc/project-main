﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Retractable : MonoBehaviour {

	public Renderer rend; //temporary for meeting 4

    private bool isRectracted;
    private bool retract;

    private void Start()
    {
        isRectracted = false;
		rend = GetComponent<Renderer>(); //temporary for meeting 4
        //rend.enabled = true;
    }

    void Update()
    {
        
    }

    public void ToggleRetraction ()
	{
		if (isRectracted) 
			Close ();
		else Open ();

        isRectracted = !isRectracted;
    }

    private void Open()
    {
		rend.enabled = false; //temporary for meeting 4
        PlayOpenAnim();                                         // Plays animation 
        gameObject.GetComponent<BoxCollider>().enabled = false; // before disabling collider
    }

    private void Close()
    {
		rend.enabled = true; //temporary for meeting 4

        gameObject.GetComponent<BoxCollider>().enabled = true;  // Plays animation 
        PlayCloseAnim();                                        // before reenabling collider
    }

    void PlayOpenAnim()
    {
        
    }

    void PlayCloseAnim()
    {
        
    }

}
