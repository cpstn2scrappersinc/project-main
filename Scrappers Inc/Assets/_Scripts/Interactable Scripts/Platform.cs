﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : Interactable
{
    public bool noSwitch = false;

    // Starting and ending locations for platform movement
    Vector3 firstLocation;
    public Vector3 secondLocation;

    // Time it takes to move between locations
    public float crossTime;
    float t = 0;

    private void Start()
    {
        firstLocation = this.transform.position;
    }

    private void Update()
    {
        // Turn this boolean on in the Inspector to make the platform move independent of switches
        if (noSwitch)
            Interact();
    }

    public override void Interact()
    {
        // Moves the platform
        t += Time.deltaTime;
        transform.position = Vector3.Lerp(firstLocation, secondLocation, Mathf.PingPong(t / crossTime, 1));
    }

    // Collision functions to ensure the player doesn't slip off the platform
    private void OnCollisionEnter(Collision collision)
    {
        collision.gameObject.GetComponent<Rigidbody>().velocity -= collision.gameObject.GetComponent<Rigidbody>().velocity;
        collision.transform.parent = this.transform;
    }
    private void OnCollisionStay(Collision collision)
    {
        collision.transform.parent = this.transform;
    }
    private void OnCollisionExit(Collision collision)
    {
        if(!collision.gameObject.GetComponent<Pickup>())
            collision.transform.parent = null;
    }
}
