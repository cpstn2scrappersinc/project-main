﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedDoor : MonoBehaviour {

    public List<DoorSwitch> switches = new List<DoorSwitch>();
    public List<GameObject> doors = new List<GameObject>();
    float timer = 0;

    // Update is called once per frame
    void Update ()
    {
        if (CheckDoors())
            OpenDoor();
	}

    // Checks if all the switches are turned on
    bool CheckDoors()
    {
        int listCount = 0;

        foreach (DoorSwitch activeSwitch in switches)
            if (activeSwitch.isOn)
                listCount++;

        return (listCount == switches.Count);
    }

    // Opens the door for a set amount of time
    void OpenDoor()
    {
        foreach(GameObject door in doors)
            door.SetActive(false);

        timer += Time.deltaTime;

        if(timer >= 3.0f)
        {

            foreach (DoorSwitch activeSwitch in switches)
                if (activeSwitch.isOn)
                    activeSwitch.isOn = false;

            foreach (GameObject door in doors)
                door.SetActive(true);

            timer = 0;
        }
    }
}
