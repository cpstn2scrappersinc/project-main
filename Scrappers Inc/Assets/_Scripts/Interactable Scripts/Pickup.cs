﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : Interactable {

	// Use this for initialization
	void Start ()
    {
        canInteract = true;
        helpSigns = GetComponentInChildren<Canvas>();
    }

    public override void Interact()
    {
        base.Interact();
        linkedObject.SetActive(!linkedObject.activeSelf);
    }
}
