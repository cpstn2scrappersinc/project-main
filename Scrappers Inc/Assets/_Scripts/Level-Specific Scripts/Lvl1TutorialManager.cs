﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lvl1TutorialManager : MonoBehaviour
{
    public List<CanvasGroup> tutorialImages = new List<CanvasGroup>();
    public int curImage;
    public GameObject spider;
    public bool spiderActivated = false;

    IEnumerator moveControlCoroutine;

	// Use this for initialization
	void Start ()
    {
        curImage = 0;

        moveControlCoroutine = FadeInOutImage(3f, tutorialImages[curImage]);

        StartCoroutine(moveControlCoroutine);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!spiderActivated && GameObject.Find("GameController").GetComponentInChildren<CharMgrScript>().activeCharacter == GameObject.Find("Spider"))
        {
            SpiderJumpInstruction();
            spiderActivated = true;
        }
    }

    // Fades images in and out
    IEnumerator FadeInOutImage(float time, CanvasGroup toShow)
    {
        float i = 0;
        float rate = 1 / time;

        while(i < 1)
        {
            toShow.alpha = i;
            i += Time.deltaTime * rate;
            yield return 0;
        }
        yield return new WaitForSeconds(time);
        toShow.gameObject.SetActive(false);
        curImage++;
        StopCoroutine(moveControlCoroutine);
    }

    // Adds the spider in the game
    void AddSpider()
    {
        spider.SetActive(true);
        GameObject.Find("GameController").GetComponentInChildren<CharMgrScript>().characters[1] = GameObject.Find("Spider");
        GameObject.Find("GameController").GetComponentInChildren<CharMgrScript>().ActivateCharUI();
    }

    // Shows the jump instruction
    void SpiderJumpInstruction()
    {
        StartCoroutine(FadeInOutImage(3f, tutorialImages[curImage]));
    }

    private void OnTriggerEnter(Collider other)
    {
        // Spawns the spider when the player collides with trigger
        if (other.tag == "Player")
            AddSpider();
    }
}
