﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeedCrate : MonoBehaviour {

    // Collision detection. Checks if units are near or not
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayableUnit>())
            other.gameObject.GetComponent<PlayableUnit>().isAtEnd = true;
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.GetComponent<PlayableUnit>())
            other.gameObject.GetComponent<PlayableUnit>().isAtEnd = true;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<PlayableUnit>())
            other.gameObject.GetComponent<PlayableUnit>().isAtEnd = false;
    }
}
