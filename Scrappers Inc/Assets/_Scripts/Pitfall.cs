﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pitfall : MonoBehaviour
{
    // Transform location to where the unfortunate units return to (if possible)
    public Transform returnPoint;

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<PlayableUnit>())
        {
            other.transform.position = returnPoint.transform.position;
            other.transform.rotation = Quaternion.identity;
        }
    }
}
