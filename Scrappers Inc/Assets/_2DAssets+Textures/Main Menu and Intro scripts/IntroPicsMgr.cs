﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class IntroPicsMgr : MonoBehaviour {

	public List<GameObject> introPics;
	public List<string> introSubs;
	public int picIndex;
	public Text subtitle;

	// Use this for initialization
	void Start () 
	{
		introPics[picIndex].SetActive(true);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void NextPic ()
	{
		if (picIndex < introPics.Count) 
		{
			introPics [picIndex].SetActive (true);

			subtitle.text = introSubs[picIndex];
			picIndex += 1;
		} 
		else 
		{
			SceneManager.LoadScene("Level 1 Puzzle 1");
		}
	}
}
